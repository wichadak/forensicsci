import sys
from openpyxl import load_workbook # for open the xlsx file

str_threshold_file = sys.argv[1]
razor_file = sys.argv[2]
locus_order = sys.argv[3]

def buildLocusOrder(ff):
    lor = list()
    fr = open(ff, "r")
    for line in fr:
        lor.append(line.strip())
    fr.close()
    return lor

def buildThresholdDict(ff):
    trd = dict()
    fr = open(ff, "r")
    for line in fr:
        tokens = line.strip().split(",")
        locus = tokens[0]
        stutter_filter = float(tokens[3])
        trd[locus] = stutter_filter
    fr.close()
    return trd

def generateTotalDepthPerLocus(ff):
    l2d = dict()
    fr = open(ff, "r")
    loc = ""
    loc_count = 0
    for line in fr:
        tokens = line.strip().split("\t")
        locus,allele = tokens[0].split(":")
        depth1 = float(tokens[3])
        depth2 = float(tokens[4])
        if locus != loc:
            if loc != "":
                l2d[loc] = loc_count
            loc_count = 0
            loc_count += depth1
            loc_count += depth2
            loc = locus
        else:
            loc_count += depth1
            loc_count += depth2
    fr.close()
    #print(l2d)
    return l2d
#=======
# main
#=======
locus_threshold = buildThresholdDict(str_threshold_file)
#print("locus_threshold  = ", locus_threshold)
totalDepths = generateTotalDepthPerLocus(razor_file)
locus_order = buildLocusOrder(locus_order)

fr = open(razor_file, "r")
autosomals = dict() 
for line in fr:
    tokens = line.strip().split("\t")
    locus, allele= tokens[0].split(":")
    #print("locus in razor = ", locus)
    read_depth1 = float(tokens[3])
    read_depth2 = float(tokens[4])
    read_depth = read_depth1 + read_depth2
    if locus_threshold.__contains__(locus):
        if totalDepths[locus] > 0 and read_depth*100/totalDepths[locus] > locus_threshold[locus]:
            if not autosomals.__contains__(locus):
                alleles = list()
                alleles.append(allele)
                autosomals[locus] = alleles
            else:
                autosomals[locus].append(allele)
        #else:
            #print("locus " + locus + " has depths = 0")
    #else:
        #print("locus " + locus + " is in not in locus_threshold!!!")
fr.close()

razor_filtered_file = razor_file.replace("_allSequences.txt", "_autosomal_str")
fw = open(razor_filtered_file, "w")

for k in locus_order:
    #print(k, sorted(autosomals[k]))A
    if k != "Amelogenin":
        ss = k + "\t"
        if k in autosomals:
            for ll in sorted(autosomals[k]):
                ss += ll + ','
            fw.write(ss[:len(ss)-1] + "\n")
        else:
            print(k + " is not in " + razor_filtered_file)
            fw.write(k + "\tNA" + "\n")

fw.close()
