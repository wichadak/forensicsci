import sys
#from openpyxl import load_workbook # for open the xlsx file
import openpyxl
import os

razor_processed_files = sys.argv[1]
idx7_idx5_to_sample_mapping_file = sys.argv[2]
forenseq_file_dir = sys.argv[3]

def buildLocusOrder(ff):
    lor = list()
    fr = open(ff, "r")
    for line in fr:
        lor.append(line.strip())
    fr.close()
    return lor

def buildRazorSTRDict(ff):
    #print("ff = ", ff)
    razorSTR = dict()
    fr = open(ff, "r")
    for line in fr:
        locus, allele = line.strip().split("\t")
        razorSTR[locus] = ",".join(sorted(allele.split(",")))
    #print(razorSTR)
    return razorSTR

def buildMappingDict(ff):
    sampleIndices = dict()
    fr = open(ff, "r")
    header = fr.readline()
    for line in fr:
        sampleName, i7Index, i5Index, sampleType, mixType = line.strip().split(",")
        key = i7Index + "-" + i5Index
        sampleIndices[key] = sampleName
    #print(sampleIndices)
    return sampleIndices
        
#=======
# main
#=======
mapping = buildMappingDict(idx7_idx5_to_sample_mapping_file)

fr = open(razor_processed_files, "r")
fw_main = open(razor_processed_files + "_summary", "w")
fw_main.write("Filename\tNo. of unmatched alleles\n")
file2Unmatched = dict()
for line in fr:
    filename = line.strip()
    print("filename = ", filename)
    fw = open(filename + "_ForenSeqCompared.txt", "w")
    fw.write("Locus\tMatched or not matched\tForenSeq\tRazor\n")
    forenseqIndex = filename.split("/")[2].split("_")[0]
    if mapping[forenseqIndex][0:2] == "59":
        razorSTRs = buildRazorSTRDict(filename)
        #print("forenseqIndex = ", forenseqIndex)
        #print("Corresponding ForenSeq = ", mapping[forenseqIndex][3:])
        startFileName = mapping[forenseqIndex][3:]
    elif mapping[forenseqIndex][0:2] == "58":
        razorSTRs = buildRazorSTRDict(filename)
        #print("forenseqIndex = ", forenseqIndex)
        #print("Corresponding ForenSeq = ", mapping[forenseqIndex][6:])
        startFileName = mapping[forenseqIndex][6:]
    prefixed = [filename for filename in os.listdir(forenseq_file_dir) if filename.startswith(startFileName)]
    #print("prefixed = ", prefixed)
    #print("forenseq_file_dir + prefixed[0] = ", forenseq_file_dir + prefixed[0])
    wb = openpyxl.load_workbook(forenseq_file_dir + prefixed[0])
    #wb.get_sheet_names()
    #print("wb sheets = ", wb)
    sheet = wb.get_sheet_by_name('Autosomal STRs')
    unmatched = 0
    for i in range(16,43):
        forenseq_locus = sheet["A" + str(i)].value
        forenseq_allele = ",".join(sorted(sheet["B" + str(i)].value.split(",")))
        #print(razorSTRs)
        if forenseq_locus in razorSTRs:
            if razorSTRs[forenseq_locus] != forenseq_allele:
                if razorSTRs[forenseq_locus] + ',' + razorSTRs[forenseq_locus] == forenseq_allele:  
                    print(forenseq_locus + "\tMatched\t" + forenseq_allele + "\t" + razorSTRs[forenseq_locus])
                    fw.write(forenseq_locus +"\tMatched\t"  + forenseq_allele + "\t" + razorSTRs[forenseq_locus] + "\n")
                
                else:
                    print(forenseq_locus + "\tUNMATCHED\t" + forenseq_allele + "\t" + razorSTRs[forenseq_locus])
                    fw.write(forenseq_locus +"\tUNMATCHED\t" + forenseq_allele + "\t" + razorSTRs[forenseq_locus] + "\n")
                    unmatched += 1
            else:
                print(forenseq_locus + "\tMatched\t" + forenseq_allele + "\t" + razorSTRs[forenseq_locus])
                fw.write(forenseq_locus + "\tMatched\t" + forenseq_allele + "\t" + razorSTRs[forenseq_locus] + "\n")
        else:
            print(forenseq_locus + " is not found in razor")
            fw.write(forenseq_locus + " is not found in razor\n")
    fw.close()
    file2Unmatched[filename] = unmatched

for k in file2Unmatched.keys():
    fw_main.write(k + "\t" + str(file2Unmatched[k]) + "\n")
fw_main.close()
