import sys
import openpyxl
import os

repeat_seq_file_dir = sys.argv[1]

files = os.listdir(repeat_seq_file_dir)
#print(files)

#filelist = [ f for f in os.listdir(repeat_seq_file_dir) if f.endswith(".seqs") ]
filelist = [ f for f in files if f.endswith(".seqs") ]
print("filelist = ", filelist)
for f in filelist:
    os.remove(repeat_seq_file_dir + f)

locus2seq = dict()

files = os.listdir(repeat_seq_file_dir)
for f in files:
    if ".DS_" not in f and f[0] != "~":
        print("f = ", repeat_seq_file_dir + f)
        wb = openpyxl.load_workbook(repeat_seq_file_dir + f)
        sheet = wb.get_sheet_by_name('Autosomal STRs')
        i = 48 
        locus = sheet["A" + str(i)].value
        allele = sheet["C" + str(i)].value
        seq = sheet["E" + str(i)].value
        while seq != None:
            if allele == "Yes":
                if not locus in locus2seq:
                    f2seqs = dict()
                    if not f in f2seqs:
                        seqs = []
                        seqs.append(seq)
                        f2seqs[f] = seqs
                        locus2seq[locus] = f2seqs
                else:
                    if not f in locus2seq[locus]:
                        seqs = []
                        seqs.append(seq)
                        f2seqs[f] = seqs
                        locus2seq[locus] = f2seqs
                    else:
                        locus2seq[locus][f].append(seq)
            i += 1
            locus = sheet["A" + str(i)].value
            allele = sheet["C" + str(i)].value
            seq = sheet["E" + str(i)].value

print(locus2seq)
locuses = locus2seq.keys()
for k in locuses:
    fw = open(repeat_seq_file_dir + k + ".seqs", "w")
    f2seqs = locus2seq[k]
    fs = f2seqs.keys()
    for f in fs:
        for seq in f2seqs[f]:
            fw.write(f + "\t" + seq + "\n")
    fw.close()
